public class Unit {
    public Unit() {}

    protected boolean isDestroyed = false;

    public void destroy() { isDestroyed = true; }
    public boolean getIfDestroyed() {
        return isDestroyed;
    }
}
