import java.util.Map;

public class ShipPart extends Unit {
    public static boolean isShipWrecked(Cell cell) {
        boolean returnStatement =  true;
	//New com
        //If cell contains ship part
        if(checkForShip(cell.getUnitType())) {
            Map<Direction, Cell> neighbours = cell.getNeighborCells();
	    //New com 2 !!
            Direction dir = Direction.NORTH;

            ///if not at the end
            if(getShipPartStatement(cell) == ShipPartStatement.SHIP_MIDDLE) {
                //go to ship end
                for (Map.Entry<Direction, Cell> entry : neighbours.entrySet()) {
                    if (checkForShip(entry.getValue().getUnitType())) {
                        dir = entry.getKey();
                    }
                }
		//New com 3
                while (cell.getNeighborCell(dir) != null){
                    if(checkForShip(cell.getNeighborCell(dir).getUnitType()))
                        cell = cell.getNeighborCell(dir);
                    else
                        break;
                }
            }
            else {
                for (Map.Entry<Direction, Cell> entry : neighbours.entrySet()) {
                    if (checkForShip(entry.getValue().getUnitType())) {
                        dir = entry.getKey().getOppositeDirection();
                        break;
                    }
                }
            }


            dir = dir.getOppositeDirection();

            int nonWreckedShipCounter = 0;
            if (cell.getUnitType() == UnitType.SHIP_PART)
                nonWreckedShipCounter++;
            while (cell.getNeighborCell(dir) != null){
                if(!checkForShip(cell.getNeighborCell(dir).getUnitType()))
                    break;
                if(cell.getNeighborCell(dir).getUnitType() == UnitType.SHIP_PART) {
                    nonWreckedShipCounter++;
                }
                cell = cell.getNeighborCell(dir);
            }
	//New com 
            if(nonWreckedShipCounter > 0)
                returnStatement = false;

        }
        //If cell doesn't contain ship part
        else
            returnStatement = false;

        return returnStatement;
    }

    private static boolean checkForShip(UnitType ut) {
        boolean returnStatement = false;

        if(ut == UnitType.SHIP_PART || ut == UnitType.BROKEN_SHIP_PART)
            returnStatement = true;

        return returnStatement;
    }

    private static ShipPartStatement getShipPartStatement(Cell cell) {
        ShipPartStatement returnStatement = ShipPartStatement.SHIP_END;
        int neighbourShipPartsCounter = 0;

        Map<Direction, Cell> neighbours = cell.getNeighborCells();

        //Cornered cell has only 2 neighbours - neighbours.size == 2
        //Cell is not in the corner
        if (neighbours.size() > 2) {
            for (Map.Entry<Direction, Cell> entry : neighbours.entrySet()) {
                if(checkForShip(entry.getValue().getUnitType())) {
                    neighbourShipPartsCounter++;
                }
            }
	//Com num 2
            if(neighbourShipPartsCounter > 1)
                returnStatement = ShipPartStatement.SHIP_MIDDLE;
        }

        return returnStatement;
    }

    private enum ShipPartStatement {
        SHIP_END,
        SHIP_MIDDLE;
    }
}
