import javax.swing.*;
import java.awt.*;

public class GameWidget extends JPanel {
    private Game game;
    private PlayerActionObserver observer = new PlayerActionObserver();
    private GameFieldWidget AIGameFieldWidget;
    private GameFieldWidget aliveGameFieldWidget;
	//First com
    private CustomModal winnerModal;

    private final WindowHandler owner;

    public GameWidget(WindowHandler owner) {
        System.out.println("HELLO");
        if(owner == null)
            throw new IllegalArgumentException("StartingWindowHandler can't be null!");
        this.owner = owner;
    }

    public void setupGame(Game game) {
        this.game = game;
        game.getPlayer().setActive(true);
        game.init();

        setBackground(Styles.PRIMARY_BACKGROUND_COLOR);
        int gridCounter = 0;
        setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(5,5,5,5);
        constraints.fill = GridBagConstraints.CENTER;
        constraints.weightx = 1.0;
        constraints.gridy = gridCounter++;
        constraints.gridx = 0;
	//Second com
        JLabel AIHeader = CustomJLabel.createCustomLabel(game.getAIPlayer().getName() + "'s field", Styles.GAME_HEADER_FONT);
        JLabel AliveHeader = CustomJLabel.createCustomLabel(game.getPlayer().getName() + "'s field", Styles.GAME_HEADER_FONT);
        AIGameFieldWidget = new GameFieldWidget(this, false, observer);
        aliveGameFieldWidget = new GameFieldWidget(this, true, observer);

        CustomJButton resetBtn = new CustomJButton("Game Reset", Styles.REGULAR_BUTTON, Styles.REGULAR_HOVERED_BUTTON);
        resetBtn.setPreferredSize(new Dimension(200, 50));
        resetBtn.setFont(Styles.PRIMARY_FONT);
        resetBtn.addActionListener(e -> handleGameReset());

        //Header for names
        constraints.gridwidth = 1;
        constraints.gridx = 0;
        add(AIHeader, constraints);

        constraints.gridwidth = 2;
        constraints.gridx = 1;
        add(AliveHeader, constraints);
        constraints.gridy = gridCounter++;

        //Fields
        constraints.gridwidth = 2;
        constraints.gridx = 0;
        add(AIGameFieldWidget, constraints);

        constraints.gridwidth = 2;
        constraints.gridx = 1;
        add(aliveGameFieldWidget, constraints);
        constraints.gridy = gridCounter++;

        //Reset
        constraints.gridwidth = 2;
        constraints.gridx = 1;
        resetBtn.addActionListener(e -> handleGameReset());
        add(resetBtn, constraints);

        setVisible(false);
    }

    public Game getGame() {
        return game;
    }

    public void handleGameReset() {
        game.abort();
        removeAll();
        setupGame(new Game(game.getFieldSize(), game.getPlayer().getName()));
        repaint();
    }

    class PlayerActionObserver implements PlayerActionListener {

        @Override
        public void playerFieldClick(PlayerActionEvent e) {
            GameStatus status = game.getGameStatus();

            if(game.getPlayer().getIfActive() && !e.getCell().isHitted()) {
                status = game.playerMakesMove(e.getCell());


                AIGameFieldWidget.hitRepaint(e.getCell());

                if(game.getAIPlayer().getPreviouslyHittedCells() != null)
                    game.getAIPlayer().getPreviouslyHittedCells().forEach(c -> {
                        aliveGameFieldWidget.hitRepaint(c);
                    });
            }


            if(status == GameStatus.WINNER_FOUND) {
                AIGameFieldWidget.getCellWidgets().forEach(cw -> {
                    cw.setEnabled(false);
                });

                //Modal window for winner
                JLabel winnerHeader = CustomJLabel.createCustomLabel(game.getWinner().getName() + " won!", Styles.GAME_HEADER_FONT);
                winnerModal = new CustomModal(owner, "YAY!", winnerHeader, 300,150);
                CustomJButton okButt = new CustomJButton("OK", Styles.REGULAR_BUTTON, Styles.REGULAR_HOVERED_BUTTON);
                okButt.addActionListener(w -> winnerModal.setVisible(false));
                winnerModal.addButton(okButt);

                winnerModal.setVisible(true);
            }

            repaint();
            revalidate();
        }
    }
}
